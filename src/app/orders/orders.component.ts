import { Component, OnInit } from '@angular/core';
import { DeleteDialogComponent } from '../shared/delete-dialog/delete-dialog.component';
import { MatDialogRef, MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { OrdersService } from './orders.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
})
export class OrdersComponent implements OnInit {
  deleteOrderDialogRef: MatDialogRef<DeleteDialogComponent>;
  // addOrderDialogRef: MatDialogRef<AddCustomerDialogComponent>;
  orders$: Observable<any>;
  displayedColumns: any = [];
  columns: any = [];

  constructor(
    private readonly ordersService: OrdersService,
    private readonly dialog: MatDialog,
    public router: Router
  ) {}

  ngOnInit() {
    this.setUpColumns();
    this.getOrders();
  }

  setUpColumns() {
    this.columns = [
      { prop: 'order_number', name: 'Order no.' },
      { prop: 'customer', name: 'Customer name' },
      { prop: 'total_price', name: 'Total price' },
      { prop: 'amount_paid', name: 'Amount paid' },
      { prop: 'amount_due', name: 'Amount due' }, // TODO: show amount in red/green if balance is due/undue
      { prop: 'status', name: 'Order status' },
    ];
    this.displayedColumns = this.columns
      .map((column) => column.prop)
      .concat(['orderDate', 'deliveryDate', 'actions']);
  }

  editOrder(row) {
    this.router.navigate(['customer/', row.customer._id, 'order']);
    this.ordersService.setOrderId(row._id);
  }

  deleteOrderDialog(row) {
    this.deleteOrderDialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        title: 'Order',
        element: row.order_number,
      },
    });

    this.deleteOrderDialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        return;
      }
      this.deleteOrder(row);
    });
  }

  getOrders() {
    this.orders$ = this.ordersService.getOrders();
  }

  deleteOrder(row) {
    this.ordersService.deleteOrder(row._id).subscribe(() => {
      this.getOrders();
    });
  }
}
