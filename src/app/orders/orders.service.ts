import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class OrdersService {
  apiUrl = 'http://localhost:5000/api/';

  private orderId = new BehaviorSubject('');
  $orderId = this.orderId.asObservable();

  constructor(private http: HttpClient) {}

  setOrderId(orderId) {
    this.orderId.next(orderId);
  }

  // get all orders
  getOrders(): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'order-list');
  }

  // add order
  addOrder(customerId: string, order) {
    return this.http.post(this.apiUrl + 'add-order/' + customerId, order);
  }

  // edit order
  editOrder(orderId: string, order) {
    return this.http.post(this.apiUrl + 'edit-order/' + orderId, order);
  }

  // delete order
  deleteOrder(id: string) {
    return this.http.delete(this.apiUrl + 'delete-order/' + id);
  }
}
