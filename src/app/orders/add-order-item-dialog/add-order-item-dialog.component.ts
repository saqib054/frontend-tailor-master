import { Component, OnInit, Inject } from '@angular/core';
import { ProductsService } from 'src/app/products/products.service';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-order-dialog',
  templateUrl: './add-order-item-dialog.component.html',
  styleUrls: ['./add-order-item-dialog.component.css'],
})
export class AddOrderItemDialogComponent implements OnInit {
  products$: Observable<any>;
  title = '';
  orderForm: FormGroup;
  selected;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AddOrderItemDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private productsService: ProductsService
  ) {}

  ngOnInit() {
    this.setUpForm();
    this.getProducts();
    this.onChanges();
  }

  setUpForm() {
    this.orderForm = this.fb.group({
      selectedProduct: '',
      price: [{ value: null, disabled: true }],
      quantity: 0,
      subTotal: [{ value: 0, disabled: true }],
      description: '',
    });
  }

  onChanges() {
    this.orderForm.get('quantity').valueChanges.subscribe((quantity) => {
      this.setSubTotal(quantity);
    });
  }

  onSelectProduct(product) {
    this.orderForm.patchValue(product);
    console.log('this.orderForm', this.orderForm.get('selectedProduct'));
    const quantity = this.orderForm.get('quantity').value;
    if (quantity === 0 || quantity === null) {
      this.orderForm.controls['quantity'].setValue(1);
    }
    this.setSubTotal(this.orderForm.get('quantity').value);
  }

  setSubTotal(quantity) {
    const productPrice = this.orderForm.get('selectedProduct').value.price;
    const subTotal = productPrice * quantity;
    this.orderForm.controls['subTotal'].setValue(subTotal);
  }

  getProducts() {
    this.products$ = this.productsService.getProducts();
  }

  onCancel() {
    this.dialogRef.close();
  }

  onSave() {
    this.dialogRef.close({
      data: {
        product: this.orderForm.get('selectedProduct').value,
        quantity: this.orderForm.get('quantity').value,
        subTotal: this.orderForm.get('subTotal').value,
      },
    });
  }
}
