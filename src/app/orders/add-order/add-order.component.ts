import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomersService } from '../../customer/customers.service';
import {
  MatDialogRef,
  MatDialog,
  MatTable,
  MatDatepickerModule,
  MatSnackBar,
} from '@angular/material';
import { AddOrderItemDialogComponent } from '../add-order-item-dialog/add-order-item-dialog.component';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { OrdersService } from '../orders.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { DeleteDialogComponent } from 'src/app/shared/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.css'],
})
export class AddOrderComponent implements OnInit {
  @ViewChild(MatTable) table: MatTable<any>;
  addOrderItemDialogRef: MatDialogRef<AddOrderItemDialogComponent>;
  deleteItemDialogRef: MatDialogRef<DeleteDialogComponent>;
  customer$: any;
  customerId;
  columns: any = [];
  tableFooterColumns: string[] = ['total'];
  date;
  picker2: MatDatepickerModule;
  totalPrice = 0;

  displayedColumns: string[] = [];
  dataSource = [];
  orderDatesForm: FormGroup;
  orderStatus = ['pending', 'ready'];

  amountPaid;
  amountDue;
  editMode: Boolean = false;
  orderId: string = null;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly customersService: CustomersService,
    private readonly ordersService: OrdersService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    public router: Router
  ) {}

  ngOnInit() {
    this.getCustomerId();
    this.getCustomer();
    this.setUpColumns();
    this.setUpOrderDatesForm();
  }

  renderTable() {
    this.table.renderRows();
    this.totalPrice = this.getTotalCost(this.dataSource);
    this.orderDatesForm.patchValue({ totalPrice: this.totalPrice });
  }

  setUpColumns() {
    this.columns = [
      { prop: 'product_name', name: 'Product' },
      { prop: 'unit_price', name: 'Unit price' },
      { prop: 'quantity', name: 'Qty.' },
      { prop: 'sub_total', name: 'Sub total' },
    ];
    this.displayedColumns = this.columns
      .map((column) => column.prop)
      .concat(['actions']);
  }

  setUpOrderDatesForm() {
    this.orderDatesForm = this.fb.group({
      orderDate: new Date(),
      deliveryDate: '',
      orderStatus: '',
    });
  }

  addOrderItemDialog() {
    this.addOrderItemDialogRef = this.dialog.open(AddOrderItemDialogComponent);

    this.addOrderItemDialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        return;
      }
      this.dataSource.push({
        id: uuidv4(),
        product_name: res.data.product.name,
        unit_price: res.data.product.price,
        quantity: res.data.quantity,
        sub_total: res.data.subTotal,
      });
      this.renderTable();
    });
  }

  editOrderItemDialog(row) {
    this.addOrderItemDialogRef = this.dialog.open(AddOrderItemDialogComponent, {
      data: {
        title: 'Edit Item',
        formValue: {
          name: row.name,
          unitPrice: row.price,
          quantity: row.quantity,
          subTotal: row.subTotal,
        },
      },
    });

    this.addOrderItemDialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        return;
      }
    });
  }

  deleteItemDialog(row) {
    this.deleteItemDialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        title: 'Product',
        element: row.name,
      },
    });

    this.deleteItemDialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        return;
      }

      this.deleteItem(row.id);
      this.renderTable();
    });
  }

  deleteItem(id) {
    this.dataSource = this.dataSource.filter((item) => item.id != id);
  }

  getCustomer() {
    this.customer$ = this.customersService.getCustomer(this.customerId);
    this.customer$.subscribe((customer) => {
      this.ordersService.$orderId.subscribe((orderId) => {
        this.orderId = orderId;
        const customerOrder = customer.orders.find(
          (order) => order._id === orderId
        );

        if (customerOrder && customerOrder.order_items) {
          this.editMode = true;
          this.dataSource = customerOrder.order_items;
          this.amountPaid = customerOrder.amount_paid;
          this.amountDue = customerOrder.amount_due;
          this.totalPrice = customerOrder.total_price;

          this.orderDatesForm.patchValue({
            orderDate: customerOrder.created_at,
            deliveryDate: customerOrder.delivery_date,
            orderStatus: customerOrder.status,
          });
        }
      });
    });
  }

  getCustomerId() {
    this.activatedRoute.params.subscribe((customer) => {
      this.customerId = customer.id;
    });
  }

  getTotalCost(dataSource) {
    return dataSource.reduce((acc, curr) => acc + curr.sub_total, 0);
  }

  onAmountPaid() {
    this.amountDue = this.totalPrice - this.amountPaid;
  }

  saveOrder() {
    const order = {
      orderItems: this.dataSource,
      orderDate: this.orderDatesForm.value.orderDate,
      deliveryDate: this.orderDatesForm.value.deliveryDate,
      totalPrice: this.totalPrice,
      amountPaid: this.amountPaid,
      amountDue: this.amountDue,
      status: this.orderDatesForm.value.orderStatus,
    };
    if (!this.editMode) {
      this.ordersService
        .addOrder(this.customerId, order)
        .pipe(
          catchError((err) => {
            const error = err.error;
            this.snackBar.open(error, null, {
              duration: 5000,
            });
            return throwError(error);
          })
        )
        .subscribe((res: any) => {
          this.snackBar.open(res.message, null, {
            duration: 5000,
          });
        });
    }

    if (this.editMode) {
      this.ordersService
        .editOrder(this.orderId, order)
        .pipe(
          catchError((err) => {
            const error = err.error;
            this.snackBar.open(error, null, {
              duration: 5000,
            });
            return throwError(error);
          })
        )
        .subscribe((res: any) => {
          this.snackBar.open(res.message, null, {
            duration: 5000,
          });
        });
    }
  }
}
