import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './employee.service';
import { Observable, throwError } from 'rxjs';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { AddEmployeeDialogComponent } from './add-employee-dialog/add-employee-dialog.component';
import { DeleteDialogComponent } from '../shared/delete-dialog/delete-dialog.component';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  deleteEmployeeDialogRef: MatDialogRef<DeleteDialogComponent>;
  addEmployeeDialogRef: MatDialogRef<AddEmployeeDialogComponent>;
  employees$: Observable<any>;
  displayedColumns: any = [];
  columns: any = [];

  constructor(
    private employeeService: EmployeeService,
    private readonly dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.setUpColumns();
    this.getEmployees();
  }

  setUpColumns() {
    this.columns = [
      { prop: 'name', name: 'Name' },
      { prop: 'contact', name: 'Contact' },
      { prop: 'salary', name: 'Salary' },
      { prop: 'position', name: 'Postion' }
    ];
    this.displayedColumns = this.columns
      .map(column => column.prop)
      .concat(['actions']);
  }

  addEmployeeDialog() {
    this.addEmployeeDialogRef = this.dialog.open(AddEmployeeDialogComponent, {
      data: {
        title: 'Add employee'
      }
    });

    this.addEmployeeDialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.addEmployee(res);
    });
  }

  editEmployeeDialog(row) {
    // TODO: add remaining fields to add and update employee
    this.addEmployeeDialogRef = this.dialog.open(AddEmployeeDialogComponent, {
      data: {
        title: 'Update employee',
        formValue: {
          name: row.name,
          contact: row.contact,
          address: row.address
        }
      }
    });

    this.addEmployeeDialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.updateEmployee(row._id, res);
    });
  }

  deleteEmployeeDialog(row) {
    this.deleteEmployeeDialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        title: 'Employee',
        element: row.name
      }
    });

    this.deleteEmployeeDialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.deleteEmployee(row._id);
    });
  }

  getEmployees() {
    this.employees$ = this.employeeService.getEmployees();
  }

  addEmployee(formValue) {
    this.employeeService
      .addEmployee(formValue)
      .pipe(
        catchError(err => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000
        });
        this.getEmployees();
      });
  }

  updateEmployee(id: string, formValue) {
    this.employeeService
      .updateEmployee(id, formValue)
      .pipe(
        catchError(err => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000
        });
        this.getEmployees();
      });
  }

  deleteEmployee(id: string) {
    this.employeeService
      .deleteEmployee(id)
      .pipe(
        catchError(err => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000
        });
        this.getEmployees();
      });
  }
}
