import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  apiUrl = 'http://localhost:5000/api/';

  constructor(private http: HttpClient) {}

  // get employee
  getEmployees() {
    return this.http.get(this.apiUrl + 'employee-list');
  }

  // add employee
  addEmployee(formValue) {
    return this.http.post(this.apiUrl + 'add-employee', formValue);
  }

  // update employee
  updateEmployee(employeeId: string, formValue) {
    return this.http.put(
      this.apiUrl + 'update-employee/' + employeeId,
      formValue
    );
  }

  // delete employee
  deleteEmployee(id: string) {
    return this.http.delete(this.apiUrl + 'delete-employee/' + id);
  }
}
