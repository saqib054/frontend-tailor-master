import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
	isLoading = false;
	userIsAuthenticated = false;

	constructor(public authService: AuthService, private router: Router) { }

	ngOnInit() {
		this.userIsAuthenticated = this.authService.getIsAuth();
		if (this.userIsAuthenticated) {
			this.router.navigate(['/']);
		}
	}

	onSignup(form: NgForm) {
		if (form.invalid) { return; }
		this.authService.createUser(form.value.email, form.value.password);
	}

}
