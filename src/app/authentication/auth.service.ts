import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { AuthData } from './auth-data.model';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  apiUrl = 'http://localhost:5000/api/';
  private token: string;
  private authStatusListener = new Subject<boolean>();
  private isAuthenticated = false;
  private tokenTimer: any;
  private userEmail: any;

  constructor(private http: HttpClient, private router: Router) {}

  getToken() {
    return this.token;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getUserEmail() {
    return this.userEmail;
  }

  createUser(email: string, password: string) {
    const authData: AuthData = { email: email, password: password };
    this.http.post(this.apiUrl + 'signup', authData).subscribe(res => {
      console.log(res);
    });
  }

  login(email: string, password: string) {
    const authData: AuthData = { email: email, password: password };
    this.http
      .post<{
        token: string;
        expiresIn: number;
        id: string;
        name: string;
        contact: number;
        address: string;
        userEmail: string;
      }>(this.apiUrl + 'login', authData)
      .subscribe(res => {
        this.token = res.token;
        const expiresInDuration = res.expiresIn;
        if (this.token) {
          console.log('res', res);
          // TODO: Show username in header
          // May be remove navigation bar at login and sign up
          // Only show navigation bar when the user is logged in
          this.userEmail = res.userEmail;
          const userId = res.id;
          this.authStatusListener.next(true);
          this.isAuthenticated = true;
          this.setAuthTimer(expiresInDuration);
          const nowDate = new Date();
          const expirationDate = new Date(
            nowDate.getTime() + expiresInDuration * 1000
          );
          console.log('expirationDate', expirationDate);
          this.saveAuthData(this.token, expirationDate, userId);
          this.router.navigate(['/']);
        }
      });
  }

  logout() {
    this.token = null;
    this.authStatusListener.next(false);
    this.isAuthenticated = false;
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(['/login']);
  }

  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return;
    }
    const nowDate = new Date();
    const expiresIn =
      authInformation.expirationDate.getTime() - nowDate.getTime();
    if (expiresIn > 0) {
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
    }
  }

  private setAuthTimer(duration: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  private saveAuthData(token: string, expirationDate: Date, userId) {
    localStorage.setItem('token', token);
    localStorage.setItem('userId', userId);
    localStorage.setItem('expiration', expirationDate.toISOString());
  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    if (!token || !expirationDate) {
      return;
    }
    const authData = { token, expirationDate: new Date(expirationDate) };
    return authData;
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
  }
}
