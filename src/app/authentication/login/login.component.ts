import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SidenavService } from 'src/app/sidenav/sidenav.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	isLoading = false;
	userIsAuthenticated = false;

	constructor(
		private authService: AuthService,
		private router: Router,
		private readonly sidenavService: SidenavService
	) { }

	ngOnInit() {
		this.userIsAuthenticated = this.authService.getIsAuth();
		if (!this.userIsAuthenticated) {
			this.sidenavService.close();
			return;
		}
		this.router.navigate(['/']);
	}

	onLogin(form: NgForm) {
		if (form.invalid) { return; }
		this.authService.login(form.value.email, form.value.password);
	}

}
