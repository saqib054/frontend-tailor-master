import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './product.model';

@Injectable()
export class ProductsService {
  apiUrl = 'http://localhost:5000/api/';

  constructor(private http: HttpClient) {}

  getProducts(): Observable<Product> {
    return this.http.get<any>(this.apiUrl + 'product-list');
  }

  // add product
  addProduct(formValue) {
    return this.http.post(this.apiUrl + 'add-product', formValue);
  }

  // update product
  updateProduct(productId: string, formValue) {
    return this.http.put(
      this.apiUrl + 'update-product/' + productId,
      formValue
    );
  }

  // delete product
  deleteProduct(productId) {
    return this.http.delete<any>(this.apiUrl + 'delete-product/' + productId);
  }
}
