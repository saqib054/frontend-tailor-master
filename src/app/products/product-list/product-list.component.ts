import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { Observable, throwError } from 'rxjs';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { DeleteDialogComponent } from '../../shared/delete-dialog/delete-dialog.component';
import { AddproductdialogComponent } from '../add-product-dialog/add-product-dialog.component';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  deleteProductDialogRef: MatDialogRef<DeleteDialogComponent>;
  addProductDialogRef: MatDialogRef<AddproductdialogComponent>;
  displayedColumns: string[] = ['name', 'price', 'description', 'actions'];
  products$: Observable<any>;

  constructor(
    private productsService: ProductsService,
    private dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.getProducts();
  }

  addProductDialog() {
    this.addProductDialogRef = this.dialog.open(AddproductdialogComponent, {
      data: {
        title: 'Add product'
      }
    });

    this.addProductDialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.addProduct(res);
    });
  }

  editProductDialog(row) {
    this.addProductDialogRef = this.dialog.open(AddproductdialogComponent, {
      data: {
        title: 'Update product',
        formValue: {
          name: row.name,
          price: row.price,
          description: row.description
        }
      }
    });

    this.addProductDialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.updateProduct(row._id, res);
    });
  }

  deleteProductDialog(row) {
    this.deleteProductDialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        title: 'Product',
        element: row.name
      }
    });

    this.deleteProductDialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.deleteProduct(row);
    });
  }

  getProducts() {
    this.products$ = this.productsService.getProducts();
  }

  addProduct(formValue) {
    this.productsService
      .addProduct(formValue)
      .pipe(
        catchError(err => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000
        });
        this.getProducts();
      });
  }

  updateProduct(productId, formValue) {
    this.productsService
      .updateProduct(productId, formValue)
      .pipe(
        catchError(err => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000
        });
        this.getProducts();
      });
  }

  deleteProduct(row) {
    this.productsService
      .deleteProduct(row._id)
      .pipe(
        catchError(err => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000
        });
        this.getProducts();
      });
  }
}
