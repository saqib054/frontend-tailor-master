import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ProductsService } from '../products.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
	selector: 'app-add-product-dialog',
	templateUrl: './add-product-dialog.component.html',
	styleUrls: ['./add-product-dialog.component.css']
})
export class AddproductdialogComponent implements OnInit {

	title = '';
	productForm: FormGroup;

	constructor(
		private fb: FormBuilder,
		private dialogRef: MatDialogRef<AddproductdialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		public productsService: ProductsService
		) {}


	ngOnInit() {
		this.title = this.data.title;
		this.setUpForm();
	}

	setUpForm() {
		this.productForm = this.fb.group({
			name: '',
			price: '',
			description: '',
		});
		if (this.data.formValue) {
			this.productForm.patchValue(this.data.formValue);
		}
	}

	onCancel() {
		this.dialogRef.close();
	}

	onSave() {
		this.dialogRef.close(this.productForm.value);
	}

}
