import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddproductdialogComponent } from './add-product-dialog.component';

describe('AddproductdialogComponent', () => {
  let component: AddproductdialogComponent;
  let fixture: ComponentFixture<AddproductdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddproductdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddproductdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
