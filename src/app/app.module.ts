import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

// services
import { CustomersService } from './customer/customers.service';
import { ProductsService } from './products/products.service';
import { AuthService } from './authentication/auth.service';
import { SidenavService } from './sidenav/sidenav.service';
import { OrdersService } from './orders/orders.service';
import { UserService } from './user/user.service';
import { EmployeeService } from './employees/employee.service';

import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatListModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatSnackBarModule,
  MatDialogModule,
  MatTabsModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatTableModule,
  MatTooltipModule,
  MatSidenavModule,
  MatSelectModule,
  MatPaginatorModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { SearchFilterPipe } from './pipes/search-filter';
import { KeysPipe } from './pipes/keys';
import { LoginComponent } from './authentication/login/login.component';
import { SignupComponent } from './authentication/signup/signup.component';
import { HeaderComponent } from './header/header.component';
import { AuthInterceptor } from './authentication/auth-interceptor';
import { DeleteDialogComponent } from './shared/delete-dialog/delete-dialog.component';
import { AddMeasurementsComponent } from './customer/add-measurements/add-measurements.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { AddproductdialogComponent } from './products/add-product-dialog/add-product-dialog.component';
import { AddCustomerDialogComponent } from './customer/add-customer-dialog/add-customer-dialog.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { OrdersComponent } from './orders/orders.component';
import { AddOrderComponent } from './orders/add-order/add-order.component';
import { AddOrderItemDialogComponent } from './orders/add-order-item-dialog/add-order-item-dialog.component';
import { UserComponent } from './user/user.component';
import { EmployeesComponent } from './employees/employees.component';
import { AddEmployeeDialogComponent } from './employees/add-employee-dialog/add-employee-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerListComponent,
    SearchFilterPipe,
    KeysPipe,
    LoginComponent,
    SignupComponent,
    HeaderComponent,
    DeleteDialogComponent,
    AddMeasurementsComponent,
    ProductListComponent,
    AddproductdialogComponent,
    AddCustomerDialogComponent,
    SidenavComponent,
    OrdersComponent,
    AddOrderComponent,
    AddOrderItemDialogComponent,
    UserComponent,
    EmployeesComponent,
    AddEmployeeDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    HttpClientModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatDialogModule,
    MatTabsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatTableModule,
    MatTooltipModule,
    MatSidenavModule,
    MatSelectModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  entryComponents: [
    DeleteDialogComponent,
    AddproductdialogComponent,
    AddCustomerDialogComponent,
    AddOrderItemDialogComponent,
    AddEmployeeDialogComponent
  ],
  providers: [
    CustomersService,
    ProductsService,
    AuthService,
    SidenavService,
    OrdersService,
    UserService,
    EmployeeService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
