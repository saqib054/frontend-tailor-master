import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustomersService } from '../../customer/customers.service';

@Component({
	selector: 'app-delete-dialog',
	templateUrl: './delete-dialog.component.html',
	styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent implements OnInit {

	title: string;
	element: string;

	constructor( private dialogRef: MatDialogRef<DeleteDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		public customersService: CustomersService) {
			this.title = data.title;
			this.element = data.element;
		}

	ngOnInit() {
	}

	onCancel() {
		this.dialogRef.close();
	}

	onDelete() {
		this.dialogRef.close(true);
	}


}
