import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { SidenavService } from './sidenav.service';
import { AuthService } from '../authentication/auth.service';

@Component({
	selector: 'app-sidenav',
	templateUrl: './sidenav.component.html',
	styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
	@ViewChild('sidenav') public sidenav: MatSidenav;
	userIsAuthenticated = false;
	opened: boolean;

	constructor(
		private sidenavService: SidenavService,
		private authService: AuthService,
		) { }

	ngOnInit() {
		this.sidenavService.setSidenav(this.sidenav);
		this.userIsAuthenticated = this.authService.getIsAuth();
		this.opened = this.userIsAuthenticated;
	}

}

