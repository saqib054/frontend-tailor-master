import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddMeasurementsComponent } from './customer/add-measurements/add-measurements.component';
import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { SignupComponent } from './authentication/signup/signup.component';
import { LoginComponent } from './authentication/login/login.component';
import { AuthGuard } from './authentication/auth.guard';
import { ProductListComponent } from './products/product-list/product-list.component';
import { OrdersComponent } from './orders/orders.component';
import { AddOrderComponent } from './orders/add-order/add-order.component';
import { UserComponent } from './user/user.component';
import { EmployeesComponent } from './employees/employees.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerListComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'add-measurements/:customerId',
    component: AddMeasurementsComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'products',
    component: ProductListComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'orders',
    component: OrdersComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'customer/:id/order',
    component: AddOrderComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'employees',
    component: EmployeesComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'user-settings',
    component: UserComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },

  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: '**', component: CustomerListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class AppRoutingModule {}
