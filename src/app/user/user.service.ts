import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl = 'http://localhost:5000/api/';

  constructor(private http: HttpClient) {}

  // get user
  getUser(userId: string) {
    return this.http.get(this.apiUrl + 'user/' + userId);
  }

  // update user
  updateUser(userId: string, formValue) {
    return this.http.put(this.apiUrl + 'update-user/' + userId, formValue);
  }
}
