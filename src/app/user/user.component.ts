import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from './user.service';
import { Observable, throwError } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  userForm: FormGroup;
  user$: Observable<any>;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.setUpForm();
    this.getUser();
  }

  setUpForm() {
    this.userForm = this.fb.group({
      name: '',
      contact: '',
      address: ''
    });
  }

  getUser() {
    const userId = localStorage.getItem('userId');
    this.user$ = this.userService.getUser(userId);
    this.user$.subscribe(res => {
      this.userForm.patchValue(res);
    });
  }

  onSave() {
    const userId = localStorage.getItem('userId');
    this.userService
      .updateUser(userId, this.userForm.value)
      .pipe(
        catchError(err => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000
        });
        this.getUser();
      });
  }
}
