import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Customer } from '../customer.model';
import { CustomersService } from '../customers.service';

@Component({
	selector: 'app-add-measurements',
	templateUrl: './add-measurements.component.html',
	styleUrls: ['./add-measurements.component.css']
})
export class AddMeasurementsComponent implements OnInit {
	customerId: string;
	customer: Customer;
	measurementForm: FormGroup;

	constructor(
		public customersService: CustomersService,
		public route: ActivatedRoute,
		public snackBar: MatSnackBar,
		private formBuilder: FormBuilder
	) { }

	ngOnInit() {
		this.loadCustomerData();
		this.shalwarQameezMeasurements();

	}
	loadCustomerData() {
		// find id using angular router

		this.route.paramMap.subscribe((paramMap: ParamMap) => {
			if (paramMap.has('customerId')) {
				this.customerId = paramMap.get('customerId');
			}
		});

		this.customersService.getCustomer(this.customerId)
			.subscribe((customerData: Customer) => {
				console.log('customerData', customerData);
				this.customer = customerData;
			});
	}

	shalwarQameezMeasurements() {
		this.measurementForm = this.formBuilder.group({
			measurementName: '',
			qameezLength: '',
			shoulder: '',
			sleeveLength: '',
			collarSize: '',
			chest: '',
			waist: '',
			ghera: '',
			shalwarLength: '',
			paincha: '',
			moda: '',
			cuffLength: '',
			stripLength: '',
			banStrip: '',
			frontPocket: false, // true/false
			sidePocket: '', // number of pockets e.g 1, 2
			shalwarPocket: false, // true/false
			gheraStyle: '', // round/straight
			cuffStyle: '', // square/folding/no preference
			otherDetails: ''
		});
	}

	addMeasurements() {
		console.log('*** this.measurementForm.value', this.measurementForm.value);
		// this.customersService.addMeasurements(this.customerId, this.measurementForm.value);
		this.customersService.addMeasurements(this.customerId, this.measurementForm.value).subscribe(() => {
			this.snackBar.open('measurements added', null, {
				duration: 5000,
			});
		});
	}


}
