import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Customer } from '../customer.model';
import { CustomersService } from '../customers.service';
import { MatSnackBar, MatDialog, MatDialogRef } from '@angular/material';
import { DeleteDialogComponent } from '../../shared/delete-dialog/delete-dialog.component';
import { Observable, throwError, fromEvent } from 'rxjs';
import { AddCustomerDialogComponent } from '../add-customer-dialog/add-customer-dialog.component';
import { Router } from '@angular/router';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  map,
} from 'rxjs/operators';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css'],
})
export class CustomerListComponent implements OnInit {
  @ViewChild('searchBox') searchBox: ElementRef;
  deleteCustomerDialogRef: MatDialogRef<DeleteDialogComponent>;
  addCustomerDialogRef: MatDialogRef<AddCustomerDialogComponent>;
  customers$: Observable<Customer[]>;
  displayedColumns: any = [];
  columns: any = [];
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 15, 25, 50, 100];
  error;

  constructor(
    public customersService: CustomersService,
    public snackBar: MatSnackBar,
    private dialog: MatDialog,
    public router: Router
  ) {}

  ngOnInit() {
    this.setUpColumns();
    this.getCustomers();
    this.searchCustomer();
  }

  setUpColumns() {
    this.columns = [
      { prop: 'customer_id', name: 'Id' },
      { prop: 'name', name: 'Name' },
      { prop: 'customer_number', name: 'Customer No.' },
      { prop: 'contact', name: 'Contact' },
    ];
    this.displayedColumns = this.columns
      .map((column) => column.prop)
      .concat(['actions']);
  }

  addCustomerDialog() {
    this.addCustomerDialogRef = this.dialog.open(AddCustomerDialogComponent, {
      data: {
        title: 'Add customer',
      },
    });

    this.addCustomerDialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        return;
      }
      this.addCustomer(res);
    });
  }

  editCustomerDialog(row) {
    this.addCustomerDialogRef = this.dialog.open(AddCustomerDialogComponent, {
      data: {
        title: 'Update product',
        formValue: {
          name: row.name,
          customer_number: row.customer_number,
          contact: row.contact,
          address: row.address,
        },
      },
    });

    this.addCustomerDialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        return;
      }
      this.updateCustomer(row._id, res);
    });
  }

  deleteCustomerDialog(row) {
    this.deleteCustomerDialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        title: 'Customer',
        element: row.name,
      },
    });

    this.deleteCustomerDialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        return;
      }
      this.deleteCustomer(row._id);
    });
  }

  getCustomers() {
    this.customers$ = this.customersService.getCustomers(
      this.pageIndex,
      this.pageSize
    );
  }

  addCustomer(formValue) {
    this.customersService
      .addCustomer(formValue)
      .pipe(
        catchError((err) => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000,
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000,
        });
        this.getCustomers();
      });
  }

  updateCustomer(id: string, formValue) {
    this.customersService
      .updateCustomer(id, formValue)
      .pipe(
        catchError((err) => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000,
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000,
        });
        this.getCustomers();
      });
  }

  deleteCustomer(id: string) {
    this.customersService
      .deleteCustomer(id)
      .pipe(
        catchError((err) => {
          const error = err.error;
          this.snackBar.open(error, null, {
            duration: 5000,
          });
          return throwError(error);
        })
      )
      .subscribe((res: any) => {
        this.snackBar.open(res.message, null, {
          duration: 5000,
        });
        this.getCustomers();
      });
  }

  addOrder(row) {
    this.router.navigate(['customer/', row._id, 'order']);
  }

  pageOptions(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getCustomers();
  }

  searchCustomer() {
    const searchInput = fromEvent(this.searchBox.nativeElement, 'input').pipe(
      map((e: KeyboardEvent) => (<HTMLInputElement>e.target).value),
      debounceTime(500),
      distinctUntilChanged()
    );

    searchInput.subscribe((searchTerm) => {
      if (!searchTerm) {
        this.getCustomers();
        return;
      }
      this.customers$ = this.customersService.searchCustomer(searchTerm).pipe(
        catchError((err) => {
          this.error = err.error;
          return throwError(err);
        })
      );
    });
    this.error = null;
  }
}
