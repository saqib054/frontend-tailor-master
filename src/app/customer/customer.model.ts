export interface Customer {
  id: string;
  name: string;
  contact: number;
  customer_number: string;
  address?: string;
  description?: string;
  date?: string;
  measurements?: [{}];
}
