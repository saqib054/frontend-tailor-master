import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustomersService } from '../customers.service';

@Component({
	selector: 'app-add-customer-dialog',
	templateUrl: './add-customer-dialog.component.html',
	styleUrls: ['./add-customer-dialog.component.css']
})
export class AddCustomerDialogComponent implements OnInit {

	title = '';
	customerForm: FormGroup;

	constructor(
		private fb: FormBuilder,
		private dialogRef: MatDialogRef<AddCustomerDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
		) {}


	ngOnInit() {
		this.title = this.data.title;
		this.setUpForm();
	}

	setUpForm() {
		this.customerForm = this.fb.group({
			name: '',
			customer_number: '',
			contact: '',
			address: '',
		});
		if (this.data.formValue) {
			this.customerForm.patchValue(this.data.formValue);
		}
	}

	onCancel() {
		this.dialogRef.close();
	}

	onSave() {
		this.dialogRef.close(this.customerForm.value);
	}

}
