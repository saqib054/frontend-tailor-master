import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from './customer.model';
import { Observable } from 'rxjs';

@Injectable()
export class CustomersService {
  apiUrl = 'http://localhost:5000/api/';

  constructor(private http: HttpClient) {}

  // get all customers
  getCustomers(pageIndex, size): Observable<Customer[]> {
    const params = {
      pageIndex: pageIndex + 1,
      size
    };
    return this.http.get<Customer[]>(this.apiUrl + 'customer-list', { params });
  }

  // get customer profile
  getCustomer(customerId: string) {
    return this.http.get(this.apiUrl + 'customer/' + customerId);
  }

  // add customer
  addCustomer(formValue) {
    return this.http.post(this.apiUrl + 'add-customer', formValue);
  }

  // add measurements
  addMeasurements(customerId: string, formValue) {
    return this.http.post(
      this.apiUrl + 'addMeasurements/' + customerId,
      formValue
    );
  }

  // update customer
  updateCustomer(customerId: string, formValue) {
    return this.http.put(
      this.apiUrl + 'update-customer/' + customerId,
      formValue
    );
  }

  // delete customer
  deleteCustomer(id: string) {
    return this.http.delete(this.apiUrl + 'delete-customer/' + id);
  }

  // search customer by name
  searchCustomer(name: string): Observable<any> {
    return this.http.get(this.apiUrl + 'customer/search/' + name);
  }
}
