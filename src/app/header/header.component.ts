import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../authentication/auth.service';
import { Subscription } from 'rxjs';
import { SidenavService } from '../sidenav/sidenav.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
	userIsAuthenticated = false;
	userName: string;
	private authListenerSubscription: Subscription;

	constructor(
		private authService: AuthService,
		private sidenavService: SidenavService
	) { }

	ngOnInit() {
		this.userIsAuthenticated = this.authService.getIsAuth();
		this.authListenerSubscription = this.authService.getAuthStatusListener()
			.subscribe(authenticationInfo => {
				this.userIsAuthenticated = authenticationInfo;
				if (!this.userIsAuthenticated) { return; }
				this.sidenavService.open();
			});
	}

	toggleSidenav() {
		this.sidenavService.toggle();
	}

	onLogout() {
		this.authService.logout();
		this.sidenavService.close();
	}

	ngOnDestroy() {
		this.authListenerSubscription.unsubscribe();
	}

}
