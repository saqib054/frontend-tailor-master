import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { isEmpty } from 'lodash';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Pipe({
	name: 'searchfilter'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
	transform(items: any[], searchText?: string): Observable<any> {
		if (!items) { return of([]); }
		if (!searchText) { return of(items); }

		searchText = searchText.toLowerCase();
		const result = items.filter(item => {
			return JSON.stringify(item).toLowerCase().includes(searchText);
		});

		console.log('result', result);
		return of(result).pipe(
			delay(500)
		);
	}
}

