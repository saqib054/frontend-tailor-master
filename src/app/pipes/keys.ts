import { PipeTransform, Pipe } from '@angular/core';
import { map, startCase, reduce, isBoolean } from 'lodash';

@Pipe({ name: 'keys' })
export class KeysPipe implements PipeTransform {
	transform(object: any, args: string[]): any {

		return reduce(object, (result, value, key) => {
			if (key !== '_id' && !isBoolean(value)) {
				result.push({ key: startCase(key), value: value });
			}
			if (value === true) {
				console.log('key', key);
				console.log('value', value);
				result.push({ key: startCase(key), value: 'Yes' });
			}
			if (value === false) {
				console.log('key', key);
				console.log('value', value);
				result.push({ key: startCase(key), value: 'No' });
			}
			return result;
		}, []);
	}
}
